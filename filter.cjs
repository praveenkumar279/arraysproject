function filter(elements,cb){
    if(!Array.isArray(elements) || typeof cb !== 'function'){
        return [];
    }

    let resultArray = [];

    for(let index=0;index < elements.length; index++){
        if(cb(elements[index],index,elements) === true){
            resultArray.push(elements[index]);
        }
        
    }
    return resultArray;
}

module.exports = filter;
function flatten(nestedArray,depth,newArray){
    if(!Array.isArray(nestedArray)){
        return [];
    }
    newArray = newArray || [];

    if(typeof depth === undefined){
        depth = 1;
    }
    else if(depth <= 0){
        return nestedArray;
    }


    for(let index=0; index < nestedArray.length; index++){
        if(Array.isArray(nestedArray[index])){
            if(depth > 1){
                flatten(nestedArray[index],depth-1,newArray);
            }
        
            else{
                let nestedArrayIndex = 0;
                while(nestedArrayIndex < nestedArray[index].length){
                    newArray.push(nestedArray[index][nestedArrayIndex]);
                    nestedArrayIndex++;
                }
            }
        }
        else if(nestedArray[index] !== undefined && nestedArray[index] !== null) {
            newArray.push(nestedArray[index]);
            }
    }
    return newArray;

    }

module.exports = flatten;
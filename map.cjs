function map(elements,cb){
    if(!Array.isArray(elements) || typeof cb !== 'function'){
        return [];
    }
    let resultArray = [];
    for(let index=0; index<elements.length; index++){
            resultArray.push(cb(elements[index],index,elements));
    }
    return resultArray;
}

module.exports = map;
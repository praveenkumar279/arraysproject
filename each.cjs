//const cb = require('./test/testEach.cjs');

function each(elements,cb){
    if(typeof elements === 'undefined' || typeof cb === 'undefined'){
        return [];
    }
    if(!Array.isArray(elements) || typeof cb !== 'function'){
        return [];
    }

    if(elements.length == 0){
        return [];
    }
    for(let index=0;index<elements.length;index++){
        obj = cb(elements[index],index);
    }
    return obj;
}



module.exports = each;
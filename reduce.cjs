//const cb = require('./test/testReduce.cjs');

function reduce(elements, cb, startingValue){
    if(!Array.isArray(elements) || typeof cb !== 'function'){
        return [];
    }

    if(elements.length == 0){
        return startingValue ? [startingValue] : [];
    }


    let result = startingValue;
    let startIndex = 0;

    if (startingValue === undefined) {
        result = elements[0];
        startIndex = 1;
    }

    for (let index = startIndex; index < elements.length; index++) {
        result = cb(result, elements[index],index,elements);
    }

  return result;
}

module.exports = reduce;
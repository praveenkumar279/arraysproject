const filter = require('../filter.cjs');

function cb(value,index,elements){
    return value%2==0;
}

const resultFilter = filter([1, 2, 3, 4, 5, 5],cb);

console.log(resultFilter);

module.exports = cb;